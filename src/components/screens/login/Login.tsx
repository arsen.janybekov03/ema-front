import React, {FC} from 'react';
import LoginForm from './loginForm/LoginForm';
import Layout from '../../layout/Layout';

const Login: FC = () => {
  return (
    <Layout>
    <div className="flex gap-10 w-full justify-center mt-10">
        <img src="/images/auth.png" alt="" />
      <LoginForm />
    </div>
    </Layout>
  );
};

export default Login;
