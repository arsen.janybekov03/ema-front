import React, {ButtonHTMLAttributes, FC, PropsWithChildren} from 'react';
import cn from 'clsx';

interface IButton extends ButtonHTMLAttributes<HTMLButtonElement> {
  variant: 'purple' | 'white';
}

const Button: FC<PropsWithChildren<IButton>> = ({children, variant, className, ...rest}) => {
  return (
    <button
      {...rest}
      className={cn(
        'rounded-lg font-normal px-5 py-4 flex justify-center items-center',
        {
          'text-white bg-purple': variant === 'purple',
          'text-gray bg-white': variant === 'white',
        },
        className
      )}
    >
      {children}
    </button>
  );
};

export default Button;
