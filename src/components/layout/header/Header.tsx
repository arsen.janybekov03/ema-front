import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

const Header = () => {
    const router = useRouter()

    return (
        <header className='shadow-sm py-2'>
            {/* <div className='container ml-auto flex items-center gap-24 '>
                <img src='/images/Logo.png' alt='' />
        <nav className='flex'>
            <ul className='flex gap-10'>
                <Link href = '/shop'>
                    <li className={router.pathname === '/shop' ? 'activ' : 'header_li'}>Магазин</li>
                </Link>
                <Link href = '/mens'>
                    <li className={router.pathname === '/mens' ? 'activ' : 'header_li'}>Мужчина</li>
                </Link>
                <Link href = '/women'>
                    <li className={router.pathname === '/women' ? 'activ' : 'header_li'}>Женщина</li>
                </Link>
                <Link href = '/'>
                    <li className='header_li'>Категории</li>
                </Link>
                <Link href = '/'>
                    <li className='header_li'>Штаны</li>
                </Link>
            </ul>
        </nav>

        <div>
            <input type="text" placeholder='Поиск'
            className='rounded-lg py-5 px-3 bg-gray-100 text-light-gray w-lg h-10'
            />
        </div>

        <div className='flex justify-end gap-3'>
            <div className='icon'>
                <img src='/heart.svg' alt='' />
            </div>
            <div className='icon'>
                <img src='/user.svg' alt='' />
            </div>
            <div className='icon'>
                <img src='/shopping-cart.svg' alt='' />
            </div>

        </div>

    </div> */}









        <div className='container ml-auto flex items-center gap-48'>
            <img src='/images/Logo.png' alt='' />
            
            <div>
            <input type="text" placeholder='Поиск'
            className='rounded-lg py-5 px-3 bg-gray-100 text-light-gray w-lg h-10'
            />
        </div>

            <div className='flex gap-2 justify-end ml-52 '>
            <Link href='/auth'>
                <button className={router.pathname === '/auth' ? 'bg-purple text-white rounded-lg w-32 h-10' : 'border text-black rounded-lg w-32 h-10' }>
                  Логин
                </button>
            </Link>
            <Link href = '/signup'>
                <button  className={router.pathname === '/signup' ? 'bg-purple text-white rounded-lg w-32 h-10' : 'border text-black rounded-lg w-32 h-10 hover:bg-purple'}>Регистрация</button>
            </Link> 
            </div>
        </div>
        </header>
    );
};

export default Header;