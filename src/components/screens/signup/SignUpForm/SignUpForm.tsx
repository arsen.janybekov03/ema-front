import Button from '@/src/components/ui/button/Button';
import React, { FC } from 'react';
import Input from './../../../ui/input/Input';
import Link from 'next/link';

const SignUpForm : FC = () => {
    return (
        <div className='flex items-center  '>
            <div className="mt-10 w-xxl h-xxl flex flex-col ">

                <div className="mt-2">
                    <h2 className="text-black-light text-2xl font-semibold">Зарегистрироваться</h2>
                    <p className='text-light-gray text-sm'>Зарегистрируйтесь, чтобы получить бесплатный доступ к любому из наших продуктов</p>
                </div>

                <div className="mt-10 flex flex-col gap-5">
                    <Button variant="white" className="btn text-purple">
                        <img src="/google.svg" alt="Google Logo" />
                        Войти через Google
                    </Button>
                    <Button variant="white" className="btn text-purple">
                        <img src="/twitter.svg" alt="Google Logo" />
                        Войти через Twitter
                    </Button>
                </div>

        <form className="mt-5 flex flex-col">
            <Input htmlFor='email' type="email" placeholder="designer@gmail.com">
                Имя пользователя или адрес электронной почты
            </Input>
            <Input htmlFor='password' type="password">
            Пароль
                <div className="flex gap-2 cursor-pointer">
                <img src="/hide.svg" alt="" />
                <p className='text-gray'>Показать</p>
                </div>
            </Input>
            <p className="text-light-gray text-sm">
                Используйте 8 или более символов, сочетающих буквы, цифры и условные обозначения
            </p>

            <div className='flex justify-start mt-5 gap-3 items-center '>
                <input type='checkbox' className=' w-4 h-4 bg-gray-500'/>
                <p className='text-sm text-light-gray'>Пользовательское соглашение и Политика конфиденциальности</p>
            </div>
            <div className='flex justify-start mt-5 gap-3 '>
                <input type='checkbox' className=' w-4 h-4 bg-gray-500'/>
                <p className='text-sm text-light-gray'>Подпишитесь на нашу ежемесячную рассылку новостей</p>
            </div>

            <Button variant="purple" className="w-40 h-12 text-lg mt-5">
                Войти
            </Button>
            <Link href={'/auth'} className='text-light-gray text-sm font-normal mt-2'>
                Есть аккаунт? Войдите!
            </Link>
        </form>

            </div>
        </div>
    );
};

export default SignUpForm;