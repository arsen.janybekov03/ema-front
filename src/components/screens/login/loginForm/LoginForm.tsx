import React from 'react';
import Button from '../../../ui/button/Button';
import Input from '../../../ui/input/Input';
import Link from 'next/link';

const LoginForm = () => {
  return (
    <div className='flex items-center mt-10 w-xxl h-xxl  flex-col '>
      <div className="mt-10 w-xxl h-xxl flex flex-col ">

        <div className="mt-2">
          <h2 className="text-black-light text-2xl font-semibold">Войти в систему</h2>
        </div>

        <div className="mt-10 flex flex-col gap-5">
          <Button variant="white" className="btn text-purple">
            <img src="/google.svg" alt="Google Logo" />
            Войти через Google
          </Button>
          <Button variant="white" className="btn text-purple">
            <img src="/twitter.svg" alt="Google Logo" />
            Войти через Twitter
          </Button>
        </div>

        <div className="mt-16 flex items-center justify-center gap-5">
          <hr className="h-1 bg-gray-300 w-lg" />
          <p className="">Или</p>
          <hr className="h-1 bg-gray-300 w-lg" />
        </div>

        <form className="mt-5 flex flex-col">
          <Input htmlFor='email' type="email" placeholder="designer@gmail.com">
            Имя пользователя или адрес электронной почты
          </Input>
          <Input htmlFor='password' type="password">
            Пароль
            <div className="flex gap-2 cursor-pointer">
              <img src="/hide.svg" alt="" />
              <p>Показать</p>
            </div>
          </Input>
          <p className="flex justify-end text-gray text-sm underline cursor-pointer">
            Забыли пароль?
          </p>

          <Button variant="purple" className="w-40 h-12 text-lg mt-5">
            Войти
          </Button>
          <Link href={'/signup'} className='text-light-gray text-sm font-normal mt-2'>
            Нет аккаунта? Зарегистрируйтесь!
          </Link>
        </form>
      </div>
    </div>
  );
};

export default LoginForm;
