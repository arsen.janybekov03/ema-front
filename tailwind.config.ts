import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
        backgroundColor: {
        'purple': '#8A33FD',
    },
     width:{
      'xxl' : '567px',
      '10xl' : '1440px',
      'lg' : '247px'
     },
     height:{
      'xxl' : '804px',
      'sm' : '58',
      '10xl' : '1063px'
     },
    textColor: {
        'black-light': '#333333',
        'purple': '#8A33FD',
        'gray' : '#3C4242',
        'light-gray' : '#666666CC'
      },
  },
  plugins: [],
}
}
export default config
