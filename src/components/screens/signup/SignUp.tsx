import React, { FC } from 'react';
import SignUpForm from './SignUpForm/SignUpForm';
import Layout from '../../layout/Layout';

const SignUp : FC = () => {
    return (
        <Layout>
        <div className="flex gap-10 w-full justify-center mt-10">
            <img src='/images/signup_bg.png' alt="" />
            <SignUpForm/>
        </div>
        </Layout>
    );
};

export default SignUp;