import React, { FC, PropsWithChildren } from 'react';

interface IInput{
    type: string,
    placeholder?: string,
    htmlFor: string
}

const Input : FC<PropsWithChildren<IInput>> = ({children,placeholder,type,htmlFor}) => {
    return (
        <div>
            <label htmlFor={htmlFor}  className="text-gray text-sm font-normal mt-10 flex justify-between">{children}</label>
            <input className='w-xxl border border-gray-300 px-5 py-4 rounded-lg' type={type} placeholder={placeholder} />
        </div>
    );
};

export default Input;